#!/bin/bash

set -e

# determine the absolute path of the executing script and the directory it is in
SCRIPT_PATH="$(readlink -f "$0")"
SCRIPT_DIRECTORY_PATH="$(dirname "$SCRIPT_PATH")"
REPOSITORY_ROOT_PATH="${SCRIPT_DIRECTORY_PATH}"
TEMP_BUILD_DATA_PATH="${SCRIPT_DIRECTORY_PATH}/.build"

# ensure working directory is the repository root
cd "${REPOSITORY_ROOT_PATH}"

# clean up, if necessary
function on_exit {
  echo "Cleaning up..."
  if [ -n "${BUILD_PATH}" ]; then
    rm -vrf "${BUILD_PATH}"
  fi
}

trap on_exit EXIT

# create docker image with the build environment
BUILD_PATH=`mktemp -d`
CONTAINER_UID=`stat -c '%u' "${REPOSITORY_ROOT_PATH}/target"` # keeping UID and GID consistent inside/outside
CONTAINER_GID=`stat -c '%g' "${REPOSITORY_ROOT_PATH}/target"` # the container circumvents permission issues
cat > "${BUILD_PATH}/Dockerfile" <<EOL
FROM alpine:3.10
RUN apk update && apk add --no-cache bash gcc git go make musl-dev
RUN addgroup -g ${CONTAINER_GID} builder && adduser -h /home/builder -s /bin/bash -D -u ${CONTAINER_UID} -G builder builder
RUN mkdir -p /project && chown -R builder:builder /project
USER builder
EOL

# build build environment
docker build -t griffinplus/autoscaling-gitlab-runner/builder "${BUILD_PATH}"

# discard old build stuff to reduce the size of the build context
# that is transferred to the docker daemon to build the image
chmod 777 -R "${TEMP_BUILD_DATA_PATH}" || true
rm -rf "${TEMP_BUILD_DATA_PATH}"

# build tools to put into the image
# (files will be copied into the 'target' directory, ready to be consumed by the next step)
docker run --rm \
  -e DUMB_INIT_VERSION=${DUMB_INIT_VERSION} \
  -e GITLAB_RUNNER_VERSION=${GITLAB_RUNNER_VERSION} \
  -e DOCKER_MACHINE_VERSION=${DOCKER_MACHINE_VERSION} \
  -e DOCKER_MACHINE_DRIVER_HETZNER_VERSION=${DOCKER_MACHINE_DRIVER_HETZNER_VERSION} \
  -v "${REPOSITORY_ROOT_PATH}":/project \
  griffinplus/autoscaling-gitlab-runner/builder \
  /bin/bash -c "
    set -e
    /project/build/dumb-init.sh
    /project/build/gitlab-runner.sh
    /project/build/docker-machine.sh
    /project/build/docker-machine-driver-hetzner.sh"

# build the image itself
docker build --tag griffinplus/autoscaling-gitlab-runner .
