#!/bin/bash

set -e

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIRECTORY_PATH=$(dirname "$SCRIPT_PATH")
REPOSITORY_ROOT_PATH="${SCRIPT_DIRECTORY_PATH}/.."
SOURCE_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/.build/src"
ARTIFACTS_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/target/usr/local/bin"

echo
echo "----------------------------------------------------------------------------------------------------"
echo "Building gitlab-runner..."
echo "----------------------------------------------------------------------------------------------------"
echo

# determine latest version of gitlab-runner, if no specific version is specified
REPO_URL="https://gitlab.com/gitlab-org/gitlab-runner.git"
if [ -z "${GITLAB_RUNNER_VERSION}" ]; then
  RELEASE_VERSION_REGEX="[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}$"
  GITLAB_RUNNER_VERSION=`git ls-remote --tags ${REPO_URL} | grep -o "refs/tags/.*" | grep -o "${RELEASE_VERSION_REGEX}" | sort -Vr | head -n1`
  echo "GITLAB_RUNNER_VERSION is not set. Installing latest version (v${GITLAB_RUNNER_VERSION})..."
else
  echo "GITLAB_RUNNER_VERSION is set. Installing version v${GITLAB_RUNNER_VERSION}..."
fi

echo
echo "Cloning repository..."
echo "--------------------------------------------------"
echo
export GOPATH="${SOURCE_DIRECTORY_PATH}/go"
mkdir -p "${GOPATH}/src/gitlab.com/gitlab-org"
cd "${GOPATH}/src/gitlab.com/gitlab-org"
rm -rf gitlab-runner
git clone --branch v${GITLAB_RUNNER_VERSION} ${REPO_URL}

echo
echo "Building..."
echo "--------------------------------------------------"
echo
# building for amd64 is suffiecient as the container only supports the amd64 architecture
cd gitlab-runner
make build BUILD_PLATFORMS="-osarch='linux/amd64'"

echo
echo "Copying build artifacts..."
echo "--------------------------------------------------"
echo
mkdir -p "${ARTIFACTS_DIRECTORY_PATH}"
cp -v out/binaries/gitlab-runner-linux-amd64 "${ARTIFACTS_DIRECTORY_PATH}/gitlab-runner"
