#!/bin/bash

set -e

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIRECTORY_PATH=$(dirname "$SCRIPT_PATH")
REPOSITORY_ROOT_PATH="${SCRIPT_DIRECTORY_PATH}/.."
SOURCE_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/.build/src"
ARTIFACTS_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/target/usr/local/bin"

echo
echo "----------------------------------------------------------------------------------------------------"
echo "Building dumb-init..."
echo "----------------------------------------------------------------------------------------------------"
echo

# determine latest version of dumb-init, if no specific version is specified
REPO_URL="https://github.com/Yelp/dumb-init.git"
if [ -z "${DUMB_INIT_VERSION}" ]; then
  RELEASE_VERSION_REGEX="[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}$"
  DUMB_INIT_VERSION=`git ls-remote --tags ${REPO_URL} | grep -o "refs/tags/.*" | grep -o "${RELEASE_VERSION_REGEX}" | sort -Vr | head -n1`
  echo "DUMB_INIT_VERSION is not set. Installing latest version (v${DUMB_INIT_VERSION})..."
else
  echo "DUMB_INIT_VERSION is set. Installing version v${DUMB_INIT_VERSION}..."
fi

echo
echo "Cloning repository..."
echo "--------------------------------------------------"
echo
mkdir -p "${SOURCE_DIRECTORY_PATH}"
cd "${SOURCE_DIRECTORY_PATH}"
rm -rf dumb-init
git clone --branch v${DUMB_INIT_VERSION} ${REPO_URL}

echo
echo "Building..."
echo "--------------------------------------------------"
echo
cd dumb-init
make

echo
echo "Copying build artifacts..."
echo "--------------------------------------------------"
echo
mkdir -p "${ARTIFACTS_DIRECTORY_PATH}"
cp -v dumb-init "${ARTIFACTS_DIRECTORY_PATH}"
