#!/bin/bash

set -e

SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIRECTORY_PATH=$(dirname "$SCRIPT_PATH")
REPOSITORY_ROOT_PATH="${SCRIPT_DIRECTORY_PATH}/.."
SOURCE_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/.build/src"
ARTIFACTS_DIRECTORY_PATH="${REPOSITORY_ROOT_PATH}/target/usr/local/bin"

echo
echo "----------------------------------------------------------------------------------------------------"
echo "Building docker-machine..."
echo "----------------------------------------------------------------------------------------------------"
echo

# determine latest version of gitlab-runner, if no specific version is specified
REPO_URL="https://github.com/docker/machine"
if [ -z "${DOCKER_MACHINE_VERSION}" ]; then
  RELEASE_VERSION_REGEX="[0-9]\{1,\}\.[0-9]\{1,\}\.[0-9]\{1,\}$"
  DOCKER_MACHINE_VERSION=`git ls-remote --tags ${REPO_URL} | grep -o "refs/tags/.*" | grep -o "${RELEASE_VERSION_REGEX}" | sort -Vr | head -n1`
  echo "DOCKER_MACHINE_VERSION is not set. Installing latest version (v${DOCKER_MACHINE_VERSION})..."
else
  echo "DOCKER_MACHINE_VERSION is set. Installing version v${DOCKER_MACHINE_VERSION}..."
fi

echo
echo "Cloning repository..."
echo "--------------------------------------------------"
echo
export GOPATH="${SOURCE_DIRECTORY_PATH}/go"
mkdir -p "${GOPATH}/src/github.com/docker"
cd "${GOPATH}/src/github.com/docker"
rm -rf machine
git clone --branch v${DOCKER_MACHINE_VERSION} ${REPO_URL}

echo
echo "Building..."
echo "--------------------------------------------------"
echo
cd machine
make build

echo
echo "Copying build artifacts..."
echo "--------------------------------------------------"
echo
mkdir -p "${ARTIFACTS_DIRECTORY_PATH}"
cp -v bin/* "${ARTIFACTS_DIRECTORY_PATH}"
