#!/bin/bash

# This function splits the comma-separated string specified in first argument.
# It supports single quotes and double quotes as well as escaping to handle the quotes properly.
# The split strings trimmed and written to stdout separated by the '\1' character to handle
# tabulator and newline characters (busybox has problems handling the '\0' character appropriately).
#
# Examples:
#
# Straight forward:
# "  Hello , World , it's me!  "
# => Token 1: |Hello|
# => Token 2: |World|
# => Token 3: |it's me!|
#
# Quoting:
# "'  Hello  ','  World,  ','  it\'s me!  '"
# => Token 1: |  Hello  |
# => Token 2: |  World,  |
# => Token 3: |  it\'s me!  |
#
# Escaping separating comma:
# "Hello, World\,, it's me!"
# => Token 1: |Hello|
# => Token 2: |World,|
# => Token 3: |it's me!|

split_csv()
{
  buffer=""
  open_quote=""
  escape=0
  while IFS= read -r -d $'\001' c ; do
    if [ $escape = 0 ]; then
      if [ "$c" = "\\" ]; then
        escape=1
      else
        if [ "$c" = "$open_quote" ]; then
          buffer="$buffer$c"
          open_quote=""
        elif [ "$open_quote" == "" ]; then
          if [[ "$c" = '"' || "$c" = "'" ]]; then
            buffer="$buffer$c"
            open_quote="$c"
          elif [ "$c" = "," ]; then
            buffer=$(printf "$buffer" | sed --regexp-extended "s/^ *(.*[^ ]) *$/\1/g" )    # trim whitspaces
            buffer=$(printf "$buffer" | sed --regexp-extended "s/^'?(.*[^'])'?$/\1/g" )    # trim single quotes
            buffer=$(printf "$buffer" | sed --regexp-extended 's/^"?(.*[^"])"?$/\1/g' )    # trim double quotes
            printf "%s\001" "$buffer"
            buffer=""
          else
            buffer="$buffer$c"
          fi
        else # quote open, keep chars as they are
          buffer="$buffer$c"
        fi
      fi
    else # escape = 1
      buffer="$buffer$c"
      escape=0
    fi
  done < <(printf "$1" | awk 'BEGIN{FS=""}{for(i=1;i<=NF;i++)printf "%s%c", $(i), 1}')

  buffer=$(printf "$buffer" | sed --regexp-extended "s/^ *(.*[^ ]) *$/\1/g" )    # trim whitspaces
  buffer=$(printf "$buffer" | sed --regexp-extended "s/^'?(.*[^'])'?$/\1/g" )    # trim single quotes
  buffer=$(printf "$buffer" | sed --regexp-extended 's/^"?(.*[^"])"?$/\1/g' )    # trim double quotes

  printf "%s\001" "$buffer"
}
