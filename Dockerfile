FROM alpine:3.10

RUN \
  apk update && \
  apk add --no-cache bash ca-certificates && \
  rm -rf /var/cache/apk/* && \
  # add non-privileged user
  adduser -h /home/gitlab-runner -s /bin/bash -D gitlab-runner && \
  # add data directory for docker machine
  mkdir -p /home/gitlab-runner/.docker/machine && \
  chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/.docker

# copy scripts and precompiled tools into the image
# the following files must be put into the 'target' directory before building the image:
# /usr/local/bin/dumb-init
# /usr/local/bin/gitlab-runner
# /usr/local/bin/docker-machine
COPY target /

# run with a non-privileged user account
USER gitlab-runner

# persist docker machine data to keep track of created machines
VOLUME /home/gitlab-runner/.docker/machine

# use SIGQUIT(3) as stop signal to allow gitlab-runner to shut down gracefully
STOPSIGNAL SIGQUIT

# expose metrics port
EXPOSE 9252/tcp

# set entrypoint and default command
ENTRYPOINT [ "/usr/local/bin/dumb-init", "--" ]
CMD [ "/app/entrypoint", "run" ]
