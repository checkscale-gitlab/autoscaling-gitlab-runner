# Configuration of the Docker Machine Driver for *Digital Ocean*

Virtual machines are called *droplets* at Digital Ocean. The following
documentation uses this term to comply with the official documentation. The
built-in driver for *Digital Ocean* is documented [here](https://docs.docker.com/machine/drivers/digital-ocean).
The variables listed below are mapped to their corresponding command line
parameters to pass them to *Docker Machine* when creating a new machine. Please
note that the default value of environment variables may differ from the one in
the *Docker Machine* documentation to better meet the needs of Gitlab runners.
Some of the values shown below may differ from region to region and also depends
on your status at Digital Ocean. Unverified accounts are not granted access to
the more expensive droplets. It's recommended to first try to create the desired
droplet manually via the web interface. If this succeeds, you can should also be
able to create them via the API, i.e. *Docker Machine* should then be able to
create them for you.

## >>> DIGITALOCEAN_ACCESS_TOKEN ***[mandatory]***

The token to access the Digital Ocean API. The access token can be created via
the [Digital Ocean Web Interface](https://www.digitalocean.com). The token must
have read/write permissions.

Maps to `--digitalocean-access-token` at docker-machine.

## >>> DIGITALOCEAN_BACKUPS

Enable Digital Ocean backups for the droplet.

Maps to `--digitalocean-backups` at docker-machine.

Default: `false`

## >>> DIGITALOCEAN_IMAGE

The name of the Digital Ocean image to use.

Maps to `--digitalocean-image` at docker-machine.

Default: `coreos-stable`

## >>> DIGITALOCEAN_IPV6

Enable IPv6 support for the droplet.

Maps to `--digitalocean-ipv6` at docker-machine.

Default: `false`

## >>> DIGITALOCEAN_PRIVATE_NETWORKING

Enable private networking support for the droplet.

Maps to `--digitalocean-private-networking` at docker-machine.

Default: `false`

## >>> DIGITALOCEAN_REGION

The region to create the droplet in, see [Regions API](https://developers.digitalocean.com/documentation/v2/#regions)
for how to get a list.

Maps to `--digitalocean-region` at docker-machine.

Available regions (retrieved: 2019/07/28):

| Slug   | Description
| :----- | :-------------------
| `ams1` | Amsterdam 1
| `ams2` | Amsterdam 2
| `ams3` | Amsterdam 3
| `blr1` | Bangalore 1
| `lon1` | London 1
| `nyc1` | New York 1
| `nyc2` | New York 2
| `nyc3` | New York 3
| `sgp1` | Singapore 1
| `sfo1` | San Francisco 1
| `sfo2` | San Francisco 2
| `tor1` | Toronto 1

Default: `nyc3`

## >>> DIGITALOCEAN_SIZE

The size of the droplet, see the [Sizes API](https://developers.digitalocean.com/documentation/v2/#list-all-sizes)
for how to get information about sizes that are supported in a certain region.

Maps to `--digitalocean-size` at docker-machine.

Available droplet sizes (retrieved: 2019/07/28):

| Standard         | General Purpose  | CPU Optimized
| :--------------- | :--------------- | :---------------------
| `s-1vcpu-1gb`    | `g-2vcpu-8gb`    | `c-2` (4 GB RAM)
| `s-1vcpu-2gb`    | `g-4vcpu-16gb`   | `c-4` (8 GB RAM)
| `s-1vcpu-3gb`    | `g-8vcpu-32gb`   | `c-8` (16 GB RAM)
| `s-2vcpu-2gb`    | `g-16vcpu-64gb`  | `c-16` (32 GB RAM)
| `s-3vcpu-1gb`    | `g-32vcpu-128gb` | `c-32` (64 GB RAM)
| `s-2vcpu-4gb`    | `g-40vcpu-160gb` |       
| `s-4vcpu-8gb`    |                  |
| `s-6vcpu-16gb`   |                  |
| `s-8vcpu-32gb`   |                  |
| `s-12vcpu-48gb`  |                  |
| `s-16vcpu-64gb`  |                  |
| `s-20vcpu-96gb`  |                  |
| `s-24vcpu-128gb` |                  |
| `s-32vcpu-192gb` |                  |

Default: `s-1vcpu-1gb`

## >>> DIGITALOCEAN_SSH_KEY_FINGERPRINT

Use an existing SSH key instead of creating a new one, see [SSH Keys](https://developers.digitalocean.com/documentation/v2/#ssh-keys).

Maps to `--digitalocean-ssh-key-fingerprint` at docker-machine.

Default: *unset*

## >>> DIGITALOCEAN_SSH_KEY_PATH

SSH private key path.

Maps to `--digitalocean-ssh-key-path` at docker-machine.

Default: *unset*

## >>> DIGITALOCEAN_SSH_PORT

SSH port.

Maps to `--digitalocean-ssh-port` at docker-machine.

Default: `22`

## >>> DIGITALOCEAN_SSH_USER

SSH username.

Maps to `--digitalocean-ssh-user` at docker-machine.

Default: `core`

## >>> DIGITALOCEAN_TAGS

Comma-separated list of tags to apply to the droplet, see [droplet tagging](https://developers.digitalocean.com/documentation/v2/#tags).

Maps to `--digitalocean-tags` at docker-machine.

Default: *unset*

## >>> DIGITALOCEAN_USERDATA

Path to file in the container containing user data for the droplet.

Maps to `--digitalocean-userdata` at docker-machine.

Default: *unset*

## >>> DIGITALOCEAN_MONITORING

Enable monitoring of droplets.

Maps to `--digitalocean-monitoring` at docker-machine.

Default: `false`
