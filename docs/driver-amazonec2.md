# Configuration of the Docker Machine Driver for *Amazon Web Services*

The [documentation](https://docs.docker.com/machine/drivers/aws) of the
*docker-machine* driver for *Amazon Web Services* presents how to configure the
driver via command line parameters and environment variables. Not all options
that can be set via command line parameters can be configured using environment
variables. The environment variables listed below are transformed to command
line parameters to add support for these settings.

## >>> AWS_ACCESS_KEY ***[mandatory]***

Your access key ID for the Amazon Web Services API.

Maps to `--amazonec2-access-key` at *docker-machine*.

## >>> AWS_AMI

The AMI ID of the instance to use.

Maps to `--amazonec2-ami` at *docker-machine*.

Default: *driver chooses a free Ubuntu 16.04 LTS image for the selected region*

## >>> AWS_BLOCK_DURATION_MINUTES

The AWS spot instance duration in minutes (60, 120, 180, 240, 300, or 360).

Maps to `--amazonec2-block-duration-minutes` at *docker-machine*.

Default: *unset*

## >>> AWS_DEFAULT_REGION

The region to use when launching the instance.

Maps to `--amazonec2-region` at *docker-machine*.

Default: `us-east-1`

## >>> AWS_DEVICE_NAME

The root device name of the instance.

Maps to `--amazonec2-device-name` at *docker-machine*.

Default: `/dev/sda1`

## >>> AWS_ENDPOINT

Optional endpoint URL (hostname only or a fully qualified URI).

Maps to `--amazonec2-endpoint` at *docker-machine*.

Default: *unset*

## >>> AWS_KEYPAIR_NAME

AWS keypair to use; requires `AWS_SSH_KEYPATH`.

Maps to `--amazonec2-keypair-name` at *docker-machine*.

Default: *unset*

## >>> AWS_INSECURE_TRANSPORT

Disable SSL when sending requests.

Maps to `--amazonec2-insecure-transport` at *docker-machine*.

Default: *unset*

## >>> AWS_INSTANCE_PROFILE

The AWS IAM role name to be used as the instance profile.

Maps to `--amazonec2-iam-instance-profile` at *docker-machine*.

Default: *unset*

## >>> AWS_INSTANCE_TYPE

The instance type to run.

Maps to `--amazonec2-instance-type` at *docker-machine*.

Default: `t2.micro`

## >>> AWS_MONITORING

Enable CloudWatch Monitoring.

Maps to `--amazonec2-monitoring` at *docker-machine*.

Default: `false`

## >>> AWS_OPEN_PORTS

Comma-separated list of port numbers to make accessible from the Internet.

Maps to `--amazonec2-open-port` at *docker-machine*.

Default: *unset*

## >>> AWS_PRIVATE_ADDRESS_ONLY

Use the private IP address only.

Default: `false`

## >>> AWS_REQUEST_SPOT_INSTANCE

Use spot instances.

Maps to `--amazonec2-request-spot-instance` at *docker-machine*.

Default: `false`

## >>> AWS_RETRIES

Set retry count for recoverable failures (use -1 to disable).

Maps to `--amazonec2-retries` at *docker-machine*.

Default: `5`

## >>> AWS_ROOT_SIZE

The root disk size of the instance (in GB).

Maps to `--amazonec2-root-size` at *docker-machine*.

Default: `16`

## >>> AWS_SECRET_ACCESS_KEY ***[mandatory]***

Your secret access key for the Amazon Web Services API.

Maps to `--amazonec2-secret-key` at *docker-machine*.

## >>> AWS_SECURITY_GROUP

The AWS VPC security group name.

Maps to `--amazonec2-security-group` at *docker-machine*.

Default: `docker-machine`

## >>> AWS_SECURITY_GROUP_READONLY

Skip adding default rules to security groups.

Maps to `--amazonec2-security-group-readonly` at *docker-machine*.

Default: `false`

## >>> AWS_SESSION_TOKEN

Your session token for the Amazon Web Services API.

Maps to `--amazonec2-session-token` at *docker-machine*.

Default: *unset*

## >>> AWS_SPOT_PRICE

Spot instance bid price in dollars.

Maps to `--amazonec2-spot-price` at *docker-machine*.

Default: `0.50`

## >>> AWS_SSH_KEYPATH ***[mandatory, if AWS_KEYPAIR_NAME is set]***

Path to private key file (in the container) to use for instance. Requires a
matching public key with .pub extension to exist.

Maps to `--amazonec2-ssh-keypath` at *docker-machine*.

Default: *unset*

## >>> AWS_SSH_USER

The SSH login username, which must match the default SSH user set in the AMI being used.

Maps to `--amazonec2-ssh-user` at *docker-machine*.

Default: `ubuntu`

## >>> AWS_SUBNET_ID ***[mandatory, if AWS_VPC_ID is not set]***

AWS VPC subnet ID.

Maps to `--amazonec2-subnet-id` at *docker-machine*.

Default: *unset*

## >>> AWS_TAGS

A comma-separated list of AWS extra tag key-value pairs, for example: `key1,value1,key2,value2`.

Maps to `--amazonec2-tags` at *docker-machine*.

Default: *unset*

## >>> AWS_USE_EBS_OPTIMIZED_INSTANCE

Create an EBS Optimized Instance. Instance type must support it.

Maps to `--amazonec2-use-ebs-optimized-instance` at *docker-machine*.

Default: `false`

## >>> AWS_USERDATA

Path (in the container) to file with cloud-init user data.

Maps to `--amazonec2-userdata` at *docker-machine*.

Default: *unset*

## >>> AWS_VOLUME_TYPE

The Amazon EBS volume type to be attached to the instance.

Maps to `--amazonec2-volume-type` at *docker-machine*.

Default: `gp2`

## >>> AWS_VPC_ID ***[mandatory, if AWS_SUBNET_ID is not set]***

Your VPC ID to launch the instance in.

Maps to `--amazonec2-vpc-id` at *docker-machine*.

## >>> AWS_ZONE

The AWS zone to launch the instance in (one of `a`, `b`, `c`, `d` and `e`).

Maps to `--amazonec2-zone` at *docker-machine*.

Default: `a`

--------------------------------------------------------------------------------

## Security Group

A security group is created and associated to the host. This security group has
the following ports opened inbound:

- ssh (22/tcp)
- docker (2376/tcp)

If you specify a security group yourself using `AWS_SECURITY_GROUP`, the above
ports are checked and opened and the security group is modified. If you want
more ports to be opened such as application-specific ports, use the AWS console
and modify the configuration manually.

## VPC ID

Your default VPC ID is determined at the start of a command. In some cases,
either because your account does not have a default VPC, or you do not want to
use the default one, you can specify a VPC with `AWS_VPS_ID`.

### To find the VPC ID:

1. Login to the AWS console.
2. Go to **Services -> VPC -> Your VPCs**.
3. Locate the VPC ID you want from the **VPC** column.
4. Go to **Services -> VPC -> Subnets**. Examine the *Availability Zone* column
   to verify that zone `a` exists and matches your VPC ID. For example,
   `us-east-1a` is in the `a` availability zone. If the `a` zone is not present,
   you can create a new subnet in that zone or specify a different zone when you
   create the machine.

## VPC Connectivity

Docker Machine uses SSH to complete the set up of instances in EC2 and requires
the ability to access the instance directly.

If you use the `AWS_PRIVATE_ADDRESS_ONLY`, ensure that you can access the new
instance from within the internal network of the VPC, such as a corporate VPN
to the VPC, a VPN instance inside the VPC, or using `docker-machine` from an
instance within your VPC.

Configuration of VPCs is beyond the scope of this guide. However, the first step
in troubleshooting is making sure that you are using private subnets that follow
the design guidance in the [AWS VPC User Guide](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenario2.html)
and have some form of NAT available so that the setup process can access the
internet to complete the setup.

## Custom AMI and SSH username

The default SSH username for the default AMIs is `ubuntu`.

You need to change the SSH username only if the custom AMI you use has a
different SSH username.

You can change the SSH username using `AWS_SSH_USER` according to the AMI you
selected with `AWS_AMI`.
